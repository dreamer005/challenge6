package com.app.tiket_bioskop.controller;

import com.app.tiket_bioskop.dto.AddFilmDto;
import com.app.tiket_bioskop.dto.request.RequestFilm;
import com.app.tiket_bioskop.dto.UpdateFilmDto;
import com.app.tiket_bioskop.entity.Films;
import com.app.tiket_bioskop.entity.Schedules;
import com.app.tiket_bioskop.service.FilmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class FilmController {

    @Autowired
    private FilmService filmService;

    @Autowired
    private  RequestFilm requestFilm;

    private static final Logger LOG = LoggerFactory.getLogger(FilmController.class);

    // NO.1 ADD FILM
    @Operation(summary = "Add new film")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Add film success!",
                    content = {@Content(schema = @Schema(example = "Add film success!"))})
    })
    @PostMapping("/film/add")
    public ResponseEntity addFilm(@RequestBody AddFilmDto addFilmDto) {
        filmService.addFilm(requestFilm.Add(addFilmDto));
        LOG.info("Add film success!");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    // NO.2 UPDATE FILM
    @Operation(summary = "Update film")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Update film success!",
                    content = {@Content(schema = @Schema(example = "Update film success!"))})
    })
    @PutMapping("/film/update")
    public ResponseEntity updateFilm(@RequestBody UpdateFilmDto updateFilmDto) {
        filmService.updateFilm(requestFilm.Update(updateFilmDto));
        LOG.info("Update film success!");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    // NO.3 DELETE FILM
    @Operation(summary = "Delete film")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Delete film success!",
                    content = {@Content(schema = @Schema(example = "Delete film success!"))})
    })
    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteFilm(@PathVariable("id") Integer filmId) {
        filmService.deleteScheduleFilm(filmId);
        filmService.deleteFilm(filmId);
        LOG.info("Delete film success!");
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    // NO.4 SHOW FILM SEDANG TAYANG
    @Operation(summary = "Show All Film Currently Showing")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Show film Success!",
                    content = {@Content(schema = @Schema(example = "Show film Success!"))})
    })
    @GetMapping("/show")
    public ResponseEntity showAllSedangTayang() {
        List<Films> allFilms = filmService.showAllFilms();
        LOG.info("Show film Success!");
        return new ResponseEntity(allFilms, HttpStatus.OK);
    };

    // NO.5 SHOW FILM SEDANG TAYANG BY ID
    @Operation(summary = "Show All Film Currently Showing By Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Show film Success!",
                    content = {@Content(schema = @Schema(example = "Show film Success!"))})
    })
    @GetMapping("/show/{id}")
    public ResponseEntity showScheduleFilm(@PathVariable("id") Integer filmId) {
        List<Schedules> schedulesById = filmService.showFilmSchedule(filmId);
        LOG.info("Show film Success!");
        return new ResponseEntity(schedulesById, HttpStatus.OK);
    }

    @Operation(summary = "Show All Film Currently Showing By Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Show film Success!",
                    content = {@Content(schema = @Schema(example = "Show film Success!"))})
    })
    @GetMapping("/show-schedule/{id}")
    public ResponseEntity showSchedule(@PathVariable("id") Integer filmId) {
        Films films = new Films(filmId);
        List<Schedules> schedulesById = filmService.showScheduleFilm(films.getFilmId());
        LOG.info("Show film Success!");
        return new ResponseEntity(schedulesById, HttpStatus.OK);
    }


}
