package com.app.tiket_bioskop.controller;

import com.app.tiket_bioskop.service.InvoiceTiketService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import net.sf.jasperreports.engine.JRException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/api")
public class InvoiceController {

    @Autowired
    private InvoiceTiketService invoiceTiketService;

    private static final Logger LOG = LoggerFactory.getLogger(InvoiceController.class);


    // PDF JASPER
    @Operation(summary = "Generate Tiket to PDF (INVOICE)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Generate Ticket Success!",
                    content = {@Content(schema = @Schema(example = "Generate Ticket Success!"))})
    })
    @GetMapping("/{userId}/{filmId}")
    public ResponseEntity generateInvoice(@PathVariable("userId") Integer userId, @PathVariable("filmId") Integer filmId) throws JRException, IOException {
        invoiceTiketService.invoice(userId, filmId);
        LOG.info("Generate Ticket Success!");
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

}
