package com.app.tiket_bioskop.controller;

import com.app.tiket_bioskop.dto.AddUserDto;
import com.app.tiket_bioskop.dto.UpdateUserDto;
import com.app.tiket_bioskop.dto.request.RequestUser;
import com.app.tiket_bioskop.service.UserServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    UserServiceImpl usersServicesImpl;

    @Autowired
    RequestUser requestUser;

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    // NO.6 CREATE USER
    @Operation(summary = "Add User")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Add user success!",
                    content = {@Content(schema = @Schema(example = "Add user success!"))})
    })

    @PostMapping("/user/create")
    public ResponseEntity addUser(@RequestBody AddUserDto addUserDto) {
        usersServicesImpl.addUser(requestUser.Add(addUserDto));
        LOG.info("Add user success!");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    // N0.7 UPDATE USER
    @Operation(summary = "Update User")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Update user success!",
                    content = {@Content(schema = @Schema(example = "Update user success!"))})
    })

    @PutMapping("/user/update")
    public ResponseEntity updateUser(@RequestBody UpdateUserDto updateUserDto) {
        usersServicesImpl.updateUser(requestUser.Update(updateUserDto));
        LOG.info("Update user success!");
        return new ResponseEntity(HttpStatus.OK);
    }

    // NO.8 DELETE USER
    @Operation(summary = "Delete user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Delete user success!",
                    content = {@Content(schema = @Schema(example = "Delete user success!"))})
    })
    @DeleteMapping("/delete-user/{id}")
    public ResponseEntity deleteUser(@PathVariable("id") Integer userId) {
        usersServicesImpl.deleteUser(userId);
        LOG.info("Delete user success!");
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

}
