package com.app.tiket_bioskop.dto;

import lombok.Getter;

@Getter
public class UpdateFilmDto {
    private Integer filmId;
    private String filmCode;
    private String filmName;
    private Integer sedangTayang;

    public UpdateFilmDto() {}

    public UpdateFilmDto(Integer filmId, String filmCode, String filmName, Integer sedangTayang) {
        this.filmId = filmId;
        this.filmCode = filmCode;
        this.filmName = filmName;
        this.sedangTayang = sedangTayang;
    }
}
