package com.app.tiket_bioskop.repository;

import com.app.tiket_bioskop.entity.user.ERole;
import com.app.tiket_bioskop.entity.user.Roles;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Roles, Integer> {
    Optional<Roles> findByName(ERole name);
}
