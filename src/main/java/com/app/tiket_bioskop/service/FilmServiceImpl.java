package com.app.tiket_bioskop.service;

import com.app.tiket_bioskop.entity.Films;
import com.app.tiket_bioskop.entity.Schedules;
import com.app.tiket_bioskop.repository.FilmRepository;
import com.app.tiket_bioskop.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmServiceImpl implements FilmService {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Override
    public void addFilm(Films films) {
        String filmCode = films.getFilmCode();
        String filmName = films.getFilmName();
        Integer sedangTayang = films.getSedangTayang();
        filmRepository.addFilm(filmCode, filmName, sedangTayang);
    }

    @Override
    public void updateFilm(Films films) {
        Integer filmId = films.getFilmId();
        String filmCode = films.getFilmCode();
        String filmName = films.getFilmName();
        Integer sedangTayang = films.getSedangTayang();
        filmRepository.updateFilm(filmCode, filmName, sedangTayang, filmId);
    }

    @Override
    public void deleteFilm(Integer filmId) {
        filmRepository.deleteFilm(filmId);
    }

    @Override
    public void deleteScheduleFilm(Integer filmId) {
        filmRepository.deleteScheduleFilm(filmId);
    }

    @Override
    public List<Films> showAllFilms() {
        return filmRepository.findAll();
    }

    @Override
    public List<Schedules> showFilmSchedule(Integer filmId) {
        return filmRepository.showFilmSchedule(filmId);
    }

    @Override
    public List<Schedules> showScheduleFilm(Integer filmId) {
        return scheduleRepository.showScheduleFilm(filmId);
    }

}
