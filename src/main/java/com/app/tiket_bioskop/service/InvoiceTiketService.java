package com.app.tiket_bioskop.service;

import com.app.tiket_bioskop.entity.Schedules;
import com.app.tiket_bioskop.entity.user.Users;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class InvoiceTiketService {

    @Autowired
    private UserService userService;

    @Autowired
    private FilmService filmService;

    @Autowired
    private HttpServletResponse httpServletResponse;

    public void invoice(Integer userId, Integer filmId) throws IOException, JRException {
        JasperPrint jasperPrint = JasperFillManager.fillReport(reportJasper(), reportParameter(), beanCollection(userId, filmId));
        responseInvoice();
        JasperExportManager.exportReportToPdfStream(jasperPrint, httpServletResponse.getOutputStream());
    }

    private void responseInvoice() {
        httpServletResponse.setContentType("application/pdf");
        httpServletResponse.addHeader("Content-Disposition", "inline; filename=InvoiceTiket.pdf;");
    }

    private List<Map<String, String>> listReport(Integer userId, Integer filmId) {
        List<Map<String, String>> dataList = new ArrayList<>();
        Map<String, String> data = new HashMap<>();

        Users users = userService.getUserById(userId);
        List<Schedules> scheduleL = filmService.showScheduleFilm(filmId);
        Schedules schedules = scheduleL.get(0);

        data.put("filmName", schedules.getFilmId().getFilmName());
        data.put("tanggalTayang", schedules.getTanggalTayang());
        data.put("jamMulai", schedules.getJamMulai());
        data.put("jamSelesai", schedules.getJamSelesai());
        data.put("hargaTiket", schedules.getHargaTiket().toString());
        data.put("username", users.getUsername());
        data.put("nomorKursi", "A1*");
        data.put("filmCode", schedules.getFilmId().getFilmCode());
        dataList.add(data);
        return dataList;
    }

    private JasperReport reportJasper() throws IOException, JRException {
        return JasperCompileManager.compileReport(ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + "InvoiceTiket.jrxml").getAbsolutePath());
    }

    private HashMap<String, Object> reportParameter() {
        HashMap<String, Object> param = new HashMap<>();
        param.put("Bioskop", "TwentyFour");
        return param;
    }

    private JRBeanCollectionDataSource beanCollection(Integer userId, Integer filmId) {
        return new JRBeanCollectionDataSource(listReport(userId, filmId));
    }

}
