package com.app.tiket_bioskop;

import com.app.tiket_bioskop.controller.FilmController;
import com.app.tiket_bioskop.dto.AddFilmDto;
import com.app.tiket_bioskop.dto.UpdateFilmDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class FilmTest {

    @Autowired
    FilmController filmController;

    @Test
    @DisplayName("1. Menambahkan film")
    void addFilm() {
        AddFilmDto addFilmDto = new AddFilmDto("KodeNo1", "NamaNo1", 1);
        filmController.addFilm(addFilmDto);
    }

    @Test
    @DisplayName("2. Mengupdate Film")
    void updateUser() {
        UpdateFilmDto updateFilmDto = new UpdateFilmDto(1,"UpdateKode2", "UpdateNama2", 0);
        filmController.updateFilm(updateFilmDto);
    }

    @Test
    @DisplayName("3. Menghapus Film")
    void deleteUser() {
        filmController.deleteFilm(1);
    }

    @Test
    @DisplayName("4. Menampilkan Film-film yang sedang tayang")
    void showSedangTayang() {
        filmController.showAllSedangTayang();
    }

    @Test
    @DisplayName("5. Menampilkan Jadwal suatu film (by id)")
    void showFilmSchedule() {
        filmController.showScheduleFilm(4);
    }

}
